package ImageAnalyzer;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.camera.core.*;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.EAN13Reader;
import com.google.zxing.oned.MultiFormatUPCEANReader;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import Listeners.ScanListener;

public class BarcodeScanner implements ImageAnalysis.Analyzer {

    private Result result;
    private MultiFormatUPCEANReader reader;
    private Hashtable<DecodeHintType, Object> hints;
    private float top;
    private float left;
    private float height;
    private float width;

    private List<ScanListener> scanListeners = new ArrayList<ScanListener>();

    public BarcodeScanner(float top, float left, float height, float width) {
        this.top = top;
        this.left = left;
        this.height = height;
        this.width = width;

        hints = new Hashtable();
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);

        reader = new MultiFormatUPCEANReader(hints);
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] byteArray = new byte[buffer.capacity()];
        buffer.get(byteArray);

        byteArray = rotateByteArray90Degrees(byteArray, image.getWidth(), image.getHeight());


        PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(byteArray,
                image.getWidth(), image.getHeight(), 0, 0,
                image.getWidth(), image.getHeight(), false);

        HybridBinarizer binarizer = new HybridBinarizer(source);

        BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);

        try {
            result = reader.decode(binaryBitmap);
            Log.d("BAR", result.getText());

            for (ScanListener listener : scanListeners){
                listener.onSuccessfulScan(result.getText());
            }

        } catch (NotFoundException e){
            result = null;
        } catch (FormatException e){
            throw new RuntimeException(e);
        }

        image.close();

    }

    private byte[] rotateByteArray90Degrees(byte[] byteArray, int width, int height){
        byte[] rotatedArray = new byte[byteArray.length];

        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                rotatedArray[x * height + height - y - 1] =
                        byteArray[x + y * width];
            }
        }

        return rotatedArray;
    }

    public void addListener(ScanListener scanListener){
        scanListeners.add(scanListener);
    }
}
