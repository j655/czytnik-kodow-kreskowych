package Listeners;

public interface ScanListener {
    void onSuccessfulScan(String barcode);
}
