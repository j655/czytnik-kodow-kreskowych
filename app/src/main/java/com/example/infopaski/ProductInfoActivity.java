package com.example.infopaski;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import API.OpenFoodFactsAPI;

public class ProductInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_info);

        Intent intent = getIntent();

        String barcode = intent.getStringExtra(MainActivity.BARCODE);

        TextView textView = findViewById(R.id.textView);

        OpenFoodFactsAPI produkt = new OpenFoodFactsAPI(barcode);
        JSONObject json = produkt.getProduct();

        try {
            if (json.getInt("status") != 0){
                try {
                    textView.setText(barcode + "\n" + json.getJSONObject("product")
                            .getString("product_name_pl"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                textView.setText(barcode + "\n" + "Nie znaleziono produktu");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}