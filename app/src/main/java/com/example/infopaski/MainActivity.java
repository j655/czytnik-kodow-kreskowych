package com.example.infopaski;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.camera.core.*;
import androidx.lifecycle.LifecycleOwner;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ImageAnalyzer.BarcodeScanner;
import Listeners.ScanListener;

public class MainActivity extends AppCompatActivity implements ScanListener {

    private final String[] REQUIRED_PERMISSIONS = new String[]{
            "android.permission.CAMERA"
    };

    private final int REQUEST_CODE_PERMISSIONS = 10;
    public static final String BARCODE = "com.example.infopaski.BARCODE";

    private ExecutorService cameraExecutor = Executors.newSingleThreadExecutor();
    private PreviewView viewFinder;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewFinder = findViewById(R.id.viewFinder);

        if (allPermissionsGranted()){
            startCamera();
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraExecutor.shutdown();
    }

    private boolean allPermissionsGranted(){
        for (String permission : REQUIRED_PERMISSIONS){
            if (ContextCompat.checkSelfPermission(getBaseContext(), permission) != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_PERMISSIONS){
            if(allPermissionsGranted()){
                startCamera();
            } else{
                Toast.makeText(this, "Brak upawnień", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }

    private void startCamera(){
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
            ProcessCameraProvider cameraProvider = null;
            try {
                cameraProvider = cameraProviderFuture.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            bindPreview(cameraProvider);
        }, ContextCompat.getMainExecutor(this));
    }

    private void bindPreview(@NonNull ProcessCameraProvider cameraProvider){
        Preview preview = new Preview.Builder().build();
        preview.setSurfaceProvider(viewFinder.getSurfaceProvider());

        float totalHeight = findViewById(R.id.viewFinder).getMeasuredHeight();
        float totalWidth = findViewById(R.id.viewFinder).getWidth();

        float top = findViewById(R.id.frameLayoutTop).getHeight() / totalHeight;
        float left = findViewById(R.id.frameLayoutLeft).getWidth() / totalWidth;

        float height = findViewById(R.id.scanArea).getHeight() / totalHeight;
        float width = findViewById(R.id.scanArea).getWidth() / totalHeight;

        ImageAnalysis imageAnalyzer = new ImageAnalysis.Builder().build();
        BarcodeScanner barcodeScanner = new BarcodeScanner(top, left, height, width);
        imageAnalyzer.setAnalyzer(cameraExecutor, barcodeScanner);

        barcodeScanner.addListener(this);

        CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build();
        try {
            cameraProvider.unbindAll();

            cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageAnalyzer);
        } catch (Exception e){
            Log.e("Error", "Use case binding failed");
        }
    }

    public void openProductInfoScreen(String barcode){
        Intent intent = new Intent(this, ProductInfoActivity.class);
        intent.putExtra(BARCODE, barcode);
        cameraExecutor.shutdown();
        startActivity(intent);
    }

    @Override
    public void onSuccessfulScan(String barcode) {
        openProductInfoScreen(barcode);
    }
}