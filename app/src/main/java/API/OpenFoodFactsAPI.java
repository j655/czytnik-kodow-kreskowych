package API;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class OpenFoodFactsAPI {

    private HttpsURLConnection connection;

    public JSONObject getProduct() {
        Log.d("JSON", product.toString());
        return product;
    }

    private JSONObject product;

    public OpenFoodFactsAPI(String productCode) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                BufferedReader reader;
                String line;
                StringBuffer responseContent = new StringBuffer();

                try {
                    URL url = new URL("https://pl.openfoodfacts.org/api/v0/product/" + productCode + ".json");
                    connection = (HttpsURLConnection) url.openConnection();

                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("User-Agent", "InfoPaski - Android - In Development");

                    connection.setConnectTimeout(5000);
                    connection.setReadTimeout(5000);

                    int status = connection.getResponseCode();

                    if (status > 299) {
                        reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                        while ((line = reader.readLine()) != null){
                            responseContent.append(line);
                        }
                        reader.close();
                    }
                    else {
                        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        while ((line = reader.readLine()) != null){
                            responseContent.append(line);
                        }
                        reader.close();
                    }

                    try {
                        product = new JSONObject(responseContent.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    connection.disconnect();
                }
            }
        });

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
